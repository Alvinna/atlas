#include <string>
#include <map>
#include <vector>
#include <random>
#include <glm/glm.hpp>
#include <atlas/visual.hpp>

Visual::Node::Node()
{

}

Visual::Node::Node(unsigned int id, glm::vec3 position, glm::vec3 color)
{
    this->id = id;
    this->position = position;
    this->color = color;
    this->size = 0.3;
}


Visual::Edge::Edge()
{

}

Visual::Edge::Edge(int id, int nid1, int nid2, EdgeType type, glm::vec3 color)
{
    this->id = id;
    this->nid1 = nid1;
    this->nid2 = nid2;
    this->type = type;
    this->color = color;
}

Visual::Scene::Scene()
{
    // Hard coded a graph to test!
    /*
    0: (0.0, 0.0, 0.0)
    1: (0.0, 0.5, 0.0)
    2: (0.5, 0.0, 0.0)
    0: 0 1
    1: 0 2
    2: 1 2
    */
   std::random_device rd; // obtain a random number from hardware
   std::mt19937 gen(rd()); // seed the generator
   std::uniform_real_distribution<>r1(-10.0, 10.0);
   std::uniform_real_distribution<>r2(0.2, 1.0);
   std::uniform_int_distribution<>r3(1, 10);
   std::uniform_real_distribution<>r4(0.005, 0.5);
    unsigned int id = 1;
   for(int i = 0; i < 1000; i++){
        network.nodes.push_back(Visual::Node(id, glm::vec3(r1(gen), r1(gen), r1(gen))));
        network.nodes[i].size = r2(gen);
        network.nodes[i].color = glm::vec3(r2(gen), r2(gen), r2(gen));
        id++;
   }
    for(int i = 0; i < 10; i++){
    network.edges.push_back(Visual::Edge(id, r3(gen), r3(gen), Visual::EDGE_DIRECTED));
    network.edges[i].size = r4(gen);
    network.edges[i].color = glm::vec3(r2(gen), r2(gen), r2(gen));
    id++;
    }

   /*
   network.nodes.push_back(Visual::Node(0, glm::vec3(0.0, 0.0, 0.0)));
   network.nodes.push_back(Visual::Node(1, glm::vec3(0.0, 1, 0.0)));
   network.nodes.push_back(Visual::Node(2, glm::vec3(1, 0.0, 0.0)));
   network.nodes[0].size = 0.8;
   network.nodes[1].size = 0.8;
   network.nodes[2].size = 0.1;
   network.nodes[0].color = glm::vec3(1.0f, 1.0f, 1.0f);
   network.nodes[1].color = glm::vec3(1.0f, 0.0f, 1.0f);
   network.nodes[2].color = glm::vec3(0.0f, 1.0f, 1.0f);
    */ 
   /*  
   network.edges.push_back(Visual::Edge(0, 0, 1, Visual::EDGE_DIRECTED));
   network.edges[0].size = 0.02;
   network.edges.push_back(Visual::Edge(1, 0, 2, Visual::EDGE_DIRECTED));
   network.edges[1].size = 0.05;
   network.edges.push_back(Visual::Edge(2, 1, 2, Visual::EDGE_DIRECTED));
   network.edges[2].size = 0.1;
*/

}