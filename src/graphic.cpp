#include <string>
#include <unordered_map>
#include <memory>
#include <QtOpenGL>
#include <atlas/graphic.hpp>

std::shared_ptr<QOpenGLShaderProgram> Graphic::ShaderManager::new_program(std::string name)
{
    std::shared_ptr<QOpenGLShaderProgram> p = std::make_shared<QOpenGLShaderProgram>();
    if(programs.find(name) == programs.end()){
        programs[name] = p;
        return p;
    }
    return nullptr;
}

bool Graphic::ShaderManager::remove_program(std::string name)
{
    if( programs.erase(name) )
        return false;
    return true;
}

bool Graphic::ShaderManager::add_shader_file(std::string name, QOpenGLShader::ShaderType type, std::string file)
{
   
    if( programs.find(name) != programs.end() )
    {
        auto p = programs[name];
        return p->addShaderFromSourceFile(type, QString::fromStdString(file));
    }
    return false;
}

bool Graphic::ShaderManager::link_shader(std::string name)
{
   
    if( programs.find(name) != programs.end() )
    {
        auto p = programs[name];
        p->link();
        return true;
    }
    return false;
}

std::shared_ptr<QOpenGLShaderProgram> Graphic::ShaderManager::get_program(std::string name)
{
    if( programs.find(name) != programs.end() )
    {
        auto p = programs[name];
        return p;
    }
    return nullptr;
}

std::shared_ptr<QOpenGLBuffer> Graphic::BufferManager::new_buffer(std::string name, QOpenGLBuffer::Type type)
{
    std::shared_ptr<QOpenGLBuffer> p = std::make_shared<QOpenGLBuffer>(type);
    if(buffers.find(name) == buffers.end()){
        buffers[name] = p;
        p->create();
        return p;
    }
    return nullptr;
}

bool Graphic::BufferManager::remove_buffer(std::string name)
{
    if( buffers.erase(name) )
        return false;
    return true;
}

std::shared_ptr<QOpenGLBuffer> Graphic::BufferManager::get_buffer(std::string name)
{
    if( buffers.find(name) != buffers.end() )
    {
        auto p = buffers[name];
        return p;
    }
    return nullptr;
}

std::shared_ptr<QOpenGLVertexArrayObject> Graphic::ArrayManager::new_array(std::string name)
{
    std::shared_ptr<QOpenGLVertexArrayObject> p = std::make_shared<QOpenGLVertexArrayObject>();
    if(arrays.find(name) == arrays.end()){
        arrays[name] = p;
        p->create();
        return p;
    }
    return nullptr;
}

bool Graphic::ArrayManager::remove_array(std::string name)
{
    if( arrays.erase(name) )
        return false;
    return true;
}

std::shared_ptr<QOpenGLVertexArrayObject> Graphic::ArrayManager::get_array(std::string name)
{
    if( arrays.find(name) != arrays.end() )
    {
        auto p = arrays[name];
        return p;
    }
    return nullptr;
}

std::shared_ptr<QOpenGLTexture> Graphic::TextureManager::new_texture(std::string name, QOpenGLTexture::Target target)
{
    std::shared_ptr<QOpenGLTexture> p = std::make_shared<QOpenGLTexture>(target);
    if(textures.find(name) == textures.end()){
        textures[name] = p;
        p->create();
        return p;
    }
    return nullptr;
}

bool Graphic::TextureManager::remove_texture(std::string name)
{
    if( textures.erase(name) )
        return false;
    return true;
}

std::shared_ptr<QOpenGLTexture> Graphic::TextureManager::get_texture(std::string name)
{
    if( textures.find(name) != textures.end() )
    {
        auto p = textures[name];
        return p;
    }
    return nullptr;
}




std::shared_ptr<QOpenGLFramebufferObject> Graphic::FrameBufferManager::new_framebuffer(std::string name, int width, int height)
{
    std::shared_ptr<QOpenGLFramebufferObject> p = std::make_shared<QOpenGLFramebufferObject>(width, height);
    if(framebuffers.find(name) == framebuffers.end()){
        framebuffers[name] = p;
        return p;
    }
    return nullptr;
}

std::shared_ptr<QOpenGLFramebufferObject> Graphic::FrameBufferManager::new_framebuffer(std::string name, int width, int height, QOpenGLFramebufferObjectFormat format)
{
    std::shared_ptr<QOpenGLFramebufferObject> p = std::make_shared<QOpenGLFramebufferObject>(width, height, format);
    if(framebuffers.find(name) == framebuffers.end()){
        framebuffers[name] = p;
        return p;
    }
    return nullptr;
}
bool Graphic::FrameBufferManager::remove_framebuffer(std::string name)
{
    auto f = framebuffers.find(name);
    if( f != framebuffers.end() )
    {
        f->second->release();
        framebuffers.erase(name);
        return false;
    }
    return true;
}

std::shared_ptr<QOpenGLFramebufferObject> Graphic::FrameBufferManager::get_framebuffer(std::string name)
{
    if( framebuffers.find(name) != framebuffers.end() )
    {
        auto p = framebuffers[name];
        return p;
    }
    return nullptr;
}