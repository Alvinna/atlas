#include <iostream>
#include <unordered_map>
#include <memory>
#include <QtCore>
#include <QtOpenGL>
#include <QColor>
#include <QOpenGLWindow>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <atlas/graphic.hpp>
#include <atlas/visual.hpp>
#include <atlas/atlas.hpp>

// RenderView
void RenderView::initializeGL()
{
    // Initialize OpenGL
    initializeOpenGLFunctions();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // These should be moved to constructor
    generatePrimitives();
    mouseMode = MOUSE_IDLE;
    lighting.Ia = glm::vec3(0.2f, 0.2f, 0.2f);
    lighting.lights[0].position = glm::vec3(5.0f, 5.0f, 5.0f);
    lighting.lights[0].Is = glm::vec3(0.5f, 0.5f, 0.5f);
    lighting.lights[0].Id = glm::vec3(0.5f, 0.5f, 0.5f);
    material.alpha = 100.0f;
    material.Ka = glm::vec3(1.0f, 1.0f, 1.0f);
    material.Ks = glm::vec3(1.0f, 1.0f, 1.0f);
    material.Kd = glm::vec3(1.0f, 1.0f, 1.0f);


    // Load various shader program

    // fakeball
    shader_mgr.new_program("fakeballs");
    shader_mgr.add_shader_file("fakeballs", QOpenGLShader::Vertex, "../shader/fakeballs.vert");
    shader_mgr.add_shader_file("fakeballs", QOpenGLShader::Fragment, "../shader/fakeballs.frag");
    shader_mgr.link_shader("fakeballs");

    buffer_mgr.new_buffer("fakeballs_id", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakeballs_index", QOpenGLBuffer::IndexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakeballs_centerPosition", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakeballs_imposterPosition", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakeballs_radius", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakeballs_color", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    
    
    // fakelines
    shader_mgr.new_program("fakelines");
    shader_mgr.add_shader_file("fakelines", QOpenGLShader::Vertex, "../shader/fakelines.vert");
    shader_mgr.add_shader_file("fakelines", QOpenGLShader::Fragment, "../shader/fakelines.frag");
    shader_mgr.link_shader("fakelines");

    buffer_mgr.new_buffer("fakelines_index", QOpenGLBuffer::IndexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakelines_id", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakelines_p1", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakelines_p2", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakelines_imposterPosition", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakelines_width", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("fakelines_color", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);

    // tex2screen
    shader_mgr.new_program("tex2screen");
    shader_mgr.add_shader_file("tex2screen", QOpenGLShader::Vertex, "../shader/tex2screen.vert");
    shader_mgr.add_shader_file("tex2screen", QOpenGLShader::Fragment, "../shader/tex2screen.frag");
    shader_mgr.link_shader("tex2screen");

    buffer_mgr.new_buffer("tex2screen_position", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);
    buffer_mgr.new_buffer("tex2screen_texcoor", QOpenGLBuffer::VertexBuffer)
        ->setUsagePattern(QOpenGLBuffer::StaticDraw);

    // testing texture
    auto tex = texture_mgr.new_texture("screenTexture", QOpenGLTexture::Target2D);
    tex->setData(QImage(QString("test.png")).mirrored());
    tex->setMinificationFilter(QOpenGLTexture::Nearest);
    tex->setMagnificationFilter(QOpenGLTexture::Nearest);
    
    glGenFramebuffers(1, &screenFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, screenFBO);
    
    glGenTextures(1, &screenTex);
    glBindTexture(GL_TEXTURE_2D, screenTex);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RG32UI, 640, 480, 0, GL_RG_INTEGER, GL_UNSIGNED_INT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glGenRenderbuffers(1, &screenDepth);
    glBindRenderbuffer(GL_RENDERBUFFER, screenDepth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 640, 480);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, screenDepth);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, screenTex, 0);

    selectionBuffer = (unsigned int*)calloc(7680 * 4320  * 2, sizeof(unsigned int)); // init big array so we don't have to resize later
    
    // VAO
    array_mgr.new_array("world");

    // Setting default view
    cameraCenter = glm::vec3(0.0, 0.0, 0.0);
    cameraRotation = glm::mat4(1);
    cameraDistance = 10.0f;
    FOV = 45.0;
    aspectRatio = 640.0 / 480.0;
    clippingPlaneFar = 400.0;
    clippingPlaneNear = 1.0;


    //Print the network for debugging
    std::cout << "Nodes" << std::endl;
    for(auto n : scene.network.nodes){
        std::cout << n.id << " " << glm::to_string(n.position) << " color:" << to_string(n.color) << std::endl;
    }
    std::cout << "Edges" << std::endl;
    for(auto n : scene.network.edges){
        std::cout << n.id << " " << n.nid1 << " -> " << n.nid2 << std::endl;
    }
}

void RenderView::resizeGL(int w, int h)
{
    aspectRatio =  (float)w / (float)h;
    
    glBindFramebuffer(GL_FRAMEBUFFER, screenFBO);
    
    glDeleteTextures(1, &screenTex);
    glGenTextures(1, &screenTex);
    glBindTexture(GL_TEXTURE_2D, screenTex);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RG32UI, w, h, 0, GL_RG_INTEGER, GL_UNSIGNED_INT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glDeleteRenderbuffers(1, &screenDepth);
    glGenRenderbuffers(1, &screenDepth);
    glBindRenderbuffer(GL_RENDERBUFFER, screenDepth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, w, h);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, screenDepth);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, screenTex, 0);

    generatePrimitives();
}

void RenderView::paintGL()
{

    GLenum bufs[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    auto vao = array_mgr.get_array("world");
    auto modelView = glm::translate(glm::mat4(1), glm::vec3(0.0f, 0.0f, -cameraDistance)) * 
        cameraRotation * glm::translate(glm::mat4(1), -cameraCenter);
    auto perspective = glm::perspective(glm::radians(FOV), aspectRatio, 
        clippingPlaneNear, clippingPlaneFar);
    auto orthographic = glm::ortho(-aspectRatio, aspectRatio, -1.0f, 1.0f);

    // Draw to texture
    //framebuffer_mgr.get_framebuffer("screen")->bind();
    glBindFramebuffer(GL_FRAMEBUFFER, screenFBO);
    //GLuint clearcolor[] = {0,0,0,0};
    //glClearBufferuiv(GL_COLOR, 0, clearcolor);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glDrawBuffers(1, bufs);
   
    // fakeballs
    {
        auto p_fakeballs = shader_mgr.get_program("fakeballs");
        auto b_fakeballs_id = buffer_mgr.get_buffer("fakeballs_id");
        auto b_fakeballs_index = buffer_mgr.get_buffer("fakeballs_index");
        auto b_fakeballs_centerPosition = buffer_mgr.get_buffer("fakeballs_centerPosition");
        auto b_fakeballs_imposterPosition = buffer_mgr.get_buffer("fakeballs_imposterPosition");
        auto b_fakeballs_radius = buffer_mgr.get_buffer("fakeballs_radius");
        auto b_fakeballs_color = buffer_mgr.get_buffer("fakeballs_color");

        vao->bind();
        p_fakeballs->bind();


        // fakeballs centerPosition
        b_fakeballs_centerPosition->bind();
        b_fakeballs_centerPosition->allocate(&primitives.fakeballs_p.centerPosition[0], 
            primitives.fakeballs_p.centerPosition.size() * sizeof(glm::vec3));
        p_fakeballs->enableAttributeArray(0);
        p_fakeballs->setAttributeBuffer(0, GL_FLOAT, 0, 3);
        // fakeballs imposterPosition
        b_fakeballs_imposterPosition->bind();
        b_fakeballs_imposterPosition->allocate(&primitives.fakeballs_p.imposterPosition[0], 
            primitives.fakeballs_p.imposterPosition.size() * sizeof(glm::vec2));
        p_fakeballs->enableAttributeArray(1);
        p_fakeballs->setAttributeBuffer(1, GL_FLOAT, 0, 2);
        // fakeballs radius
        b_fakeballs_radius->bind();
        b_fakeballs_radius->allocate(&primitives.fakeballs_p.radius[0], 
            primitives.fakeballs_p.radius.size() * sizeof(float));
        p_fakeballs->enableAttributeArray(2);
        p_fakeballs->setAttributeBuffer(2, GL_FLOAT, 0, 1);
        // fakeballs color
        b_fakeballs_color->bind();
        b_fakeballs_color->allocate(&primitives.fakeballs_p.color[0], 
            primitives.fakeballs_p.color.size() * sizeof(glm::vec3));
        p_fakeballs->enableAttributeArray(3);
        p_fakeballs->setAttributeBuffer(3, GL_FLOAT, 0, 3);
        // fakeballs id
        b_fakeballs_id->bind();
        b_fakeballs_id->allocate(&primitives.fakeballs_p.id[0], 
            primitives.fakeballs_p.id.size() * sizeof(unsigned int));
        p_fakeballs->enableAttributeArray(4);
        //p_fakeballs->setAttributeBuffer(4, GL_UNSIGNED_INT, 0, 1);
        glVertexAttribIPointer(4, 1, GL_UNSIGNED_INT, 0, 0);

        // index 
        b_fakeballs_index->bind();
        b_fakeballs_index->allocate(&(primitives.fakeballs_p.index[0]), 
            primitives.fakeballs_p.index.size() * sizeof(int));
        // global
        p_fakeballs->setUniformValue("mv_matrix", QMatrix4x4(glm::value_ptr(modelView)).transposed());
        p_fakeballs->setUniformValue("p_matrix", QMatrix4x4(glm::value_ptr(perspective)).transposed());
        p_fakeballs->setUniformValue("o_matrix", QMatrix4x4(glm::value_ptr(orthographic)).transposed());
        p_fakeballs->setUniformValue("cameraPosition", QVector3D(0.0f, 0.0f, 0.0f));

        p_fakeballs->setUniformValue("lighting.Ia", QVector3D(lighting.Ia.r, lighting.Ia.g, lighting.Ia.b));

        p_fakeballs->setUniformValue("lighting.lights[0].position", 
                QVector3D(lighting.lights[0].position.x, lighting.lights[0].position.y, lighting.lights[0].position.z));
        p_fakeballs->setUniformValue("lighting.lights[0].Is", 
                QVector3D(lighting.lights[0].Is.r, lighting.lights[0].Is.g, lighting.lights[0].Is.b));
        p_fakeballs->setUniformValue("lighting.lights[0].Id", 
                QVector3D(lighting.lights[0].Id.r, lighting.lights[0].Id.g, lighting.lights[0].Id.b));
       
        p_fakeballs->setUniformValue("material.Ka", 
                QVector3D(material.Ka.r, material.Ka.g, material.Ka.b));
        p_fakeballs->setUniformValue("material.Ks", 
                QVector3D(material.Ks.r, material.Ks.g, material.Ks.b));
        p_fakeballs->setUniformValue("material.Kd", 
                QVector3D(material.Kd.r, material.Kd.g, material.Kd.b));
        p_fakeballs->setUniformValue("material.alpha", material.alpha);
        

        //glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, framebuffer_mgr.get_framebuffer("screen")->texture(), 0);

        // indexed draw
        glDrawElements(GL_TRIANGLES, primitives.fakeballs_p.index.size(), GL_UNSIGNED_INT, 0);

        //glFlush();

        // release
        vao->release();
        b_fakeballs_id->release();
        b_fakeballs_index->release();
        b_fakeballs_centerPosition->release();
        b_fakeballs_imposterPosition->release();
        b_fakeballs_radius->release();
        b_fakeballs_color->release();
        p_fakeballs->release();
    }


    /*
        // fakelines
    
    {
        auto p_fakelines = shader_mgr.get_program("fakelines");
        auto b_fakelines_index = buffer_mgr.get_buffer("fakelines_index");
        auto b_fakelines_id = buffer_mgr.get_buffer("fakelines_id");        
        auto b_fakelines_p1 = buffer_mgr.get_buffer("fakelines_p1");        
        auto b_fakelines_p2 = buffer_mgr.get_buffer("fakelines_p2");
        auto b_fakelines_imposterPosition = buffer_mgr.get_buffer("fakelines_imposterPosition");
        auto b_fakelines_width = buffer_mgr.get_buffer("fakelines_width");
        auto b_fakelines_color = buffer_mgr.get_buffer("fakelines_color");


        vao->bind();
        p_fakelines->bind();

        // fakelines p1
        b_fakelines_p1->bind();
        b_fakelines_p1->allocate(&primitives.fakelines_p.p1[0], 
            primitives.fakelines_p.p1.size() * sizeof(glm::vec3));
        p_fakelines->enableAttributeArray(0);
        p_fakelines->setAttributeBuffer(0, GL_FLOAT, 0, 3);
        // fakelines p2
        b_fakelines_p2->bind();
        b_fakelines_p2->allocate(&primitives.fakelines_p.p2[0], 
            primitives.fakelines_p.p2.size() * sizeof(glm::vec3));
        p_fakelines->enableAttributeArray(1);
        p_fakelines->setAttributeBuffer(1, GL_FLOAT, 0, 3);
        // fakelines imposterPosition
        b_fakelines_imposterPosition->bind();
        b_fakelines_imposterPosition->allocate(&primitives.fakelines_p.imposterPosition[0], 
            primitives.fakelines_p.imposterPosition.size() * sizeof(glm::vec2));
        p_fakelines->enableAttributeArray(2);
        p_fakelines->setAttributeBuffer(2, GL_FLOAT, 0, 2);
        // fakelines radius
        b_fakelines_width->bind();
        b_fakelines_width->allocate(&primitives.fakelines_p.width[0], 
            primitives.fakelines_p.width.size() * sizeof(float));
        p_fakelines->enableAttributeArray(3);
        p_fakelines->setAttributeBuffer(3, GL_FLOAT, 0, 1);
        // fakelines color
        b_fakelines_color->bind();
        b_fakelines_color->allocate(&primitives.fakelines_p.color[0], 
            primitives.fakelines_p.color.size() * sizeof(glm::vec3));
        p_fakelines->enableAttributeArray(4);
        p_fakelines->setAttributeBuffer(4, GL_FLOAT, 0, 3);
        // fakelines id
        b_fakelines_id->bind();
        b_fakelines_id->allocate(&primitives.fakelines_p.id[0], 
            primitives.fakelines_p.id.size() * sizeof(unsigned int));
        p_fakelines->enableAttributeArray(5);
        glVertexAttribIPointer(5, 1, GL_UNSIGNED_INT, 0, 0);

        // index 
        b_fakelines_index->bind();
        b_fakelines_index->allocate(&(primitives.fakelines_p.index[0]), 
            primitives.fakelines_p.index.size() * sizeof(int));

        // global
        p_fakelines->setUniformValue("mv_matrix", QMatrix4x4(glm::value_ptr(modelView)).transposed());
        p_fakelines->setUniformValue("p_matrix", QMatrix4x4(glm::value_ptr(perspective)).transposed());
        p_fakelines->setUniformValue("o_matrix", QMatrix4x4(glm::value_ptr(orthographic)).transposed());
        p_fakelines->setUniformValue("cameraPosition", QVector3D(0.0f, 0.0f, 0.0f));

        p_fakelines->setUniformValue("lighting.Ia", QVector3D(lighting.Ia.r, lighting.Ia.g, lighting.Ia.b));

        p_fakelines->setUniformValue("lighting.lights[0].position", 
                QVector3D(lighting.lights[0].position.x, lighting.lights[0].position.y, lighting.lights[0].position.z));
        p_fakelines->setUniformValue("lighting.lights[0].Is", 
                QVector3D(lighting.lights[0].Is.r, lighting.lights[0].Is.g, lighting.lights[0].Is.b));
        p_fakelines->setUniformValue("lighting.lights[0].Id", 
                QVector3D(lighting.lights[0].Id.r, lighting.lights[0].Id.g, lighting.lights[0].Id.b));
       
        p_fakelines->setUniformValue("material.Ka", 
                QVector3D(material.Ka.r, material.Ka.g, material.Ka.b));
        p_fakelines->setUniformValue("material.Ks", 
                QVector3D(material.Ks.r, material.Ks.g, material.Ks.b));
        p_fakelines->setUniformValue("material.Kd", 
                QVector3D(material.Kd.r, material.Kd.g, material.Kd.b));
        p_fakelines->setUniformValue("material.alpha", material.alpha);

        // indexed draw
        glDrawElements(GL_TRIANGLES, primitives.fakelines_p.index.size(), GL_UNSIGNED_INT, 0);

        // release
        vao->release();
        b_fakelines_index->release();
        b_fakelines_p1->release();
        b_fakelines_p2->release();
        b_fakelines_imposterPosition->release();
        b_fakelines_width->release();
        b_fakelines_color->release();
        b_fakelines_id->release();
        p_fakelines->release();
    }
    */
    glReadPixels(0, 0, width(), height(), GL_RG_INTEGER, GL_UNSIGNED_INT, selectionBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


        // render texture to screen
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glDrawBuffers(1, bufs);

    {
        auto p_tex2screen = shader_mgr.get_program("tex2screen");
        auto b_tex2screen_position = buffer_mgr.get_buffer("tex2screen_position");
        auto b_tex2screen_texcoor = buffer_mgr.get_buffer("tex2screen_texcoor");

        vao->bind();
        p_tex2screen->bind();

        // position
        b_tex2screen_position->bind();
        b_tex2screen_position->allocate(&primitives.tex2screen_p.position[0], 
            primitives.tex2screen_p.position.size() * sizeof(glm::vec3));
        p_tex2screen->enableAttributeArray(0);
        p_tex2screen->setAttributeBuffer(0, GL_FLOAT, 0, 3);
        // texcoor
        b_tex2screen_texcoor->bind();
        b_tex2screen_texcoor->allocate(&primitives.tex2screen_p.texcoor[0], 
            primitives.tex2screen_p.texcoor.size() * sizeof(glm::vec2));
        p_tex2screen->enableAttributeArray(1);
        p_tex2screen->setAttributeBuffer(1, GL_FLOAT, 0, 2);

        // global
        p_tex2screen->setUniformValue("o_matrix", QMatrix4x4(glm::value_ptr(orthographic)).transposed());
        p_tex2screen->setUniformValue("screenTexture", 0);
        GLuint p = p_tex2screen->programId();
        glUniform1ui(glGetUniformLocation(p, "selection"), selection);
        
        // Texture
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, screenTex);

        // indexed draw
        glDrawArrays (GL_TRIANGLES, 0, primitives.tex2screen_p.position.size());

        glBindTexture(GL_TEXTURE_2D, 0);
        // release
        vao->release();
        b_tex2screen_position->release();
        b_tex2screen_texcoor->release();
        p_tex2screen->release();
    }

}

void RenderView::mousePressEvent(QMouseEvent* event)
{
    auto button = event->button();
    switch(mouseMode){
        case MOUSE_IDLE:
            // store old value
            mousePosition.x = event->x();
            mousePosition.y = event->y();


            switch(button){
                case Qt::LeftButton:
                    mouseMode = MOUSE_ROTATE;
                    break;
                case Qt::RightButton:
                    mouseMode = MOUSE_MOVEZ;
                    break;
                case Qt::MiddleButton:
                    mouseMode = MOUSE_MOVEXY;
                    break;
            }
        break;
        
        case MOUSE_MOVEXY:
        break;

        case MOUSE_MOVEZ:
        break;

        case MOUSE_ROTATE:
        break;
    }
    update();
}

void RenderView::mouseReleaseEvent(QMouseEvent* event)
{
    auto button = event->button();
    mouseMode = MOUSE_IDLE;
    update();
}

void RenderView::mouseMoveEvent(QMouseEvent* event)
{
    auto button = event->button();
    auto x = event->x();
    auto y = event->y();
    float dx = (float)(x - mousePosition.x);
    float dy = (float)(y - mousePosition.y);
    mousePosition.x = x;
    mousePosition.y = y;
    if( y < height() && x < width())
        selection = selectionBuffer[((height() - y) * width() + x) * 2 + 1];

    switch(mouseMode){
        case MOUSE_IDLE:
            std::cout << selection << std::endl;
            
        break;
        
        case MOUSE_MOVEXY:
            cameraCenter += glm::vec3(glm::vec4(-dx * 0.01f, dy * 0.01f, 0.0f, 0.0f) * cameraRotation);
        break;

        case MOUSE_MOVEZ:
            cameraDistance = cameraDistance - dy * 0.01f;
        break;

        case MOUSE_ROTATE:
            cameraRotation = glm::rotate(glm::rotate(glm::mat4(1), dy * 0.005f, glm::vec3(1.0f, 0.0f, 0.0f))
                                , dx * 0.005f, glm::vec3(0.0f, 1.0f, 0.0f)) * cameraRotation;

        break;
    }
    update();
}



void RenderView::generatePrimitives()
{
    // nodes
    int numNodes = scene.network.nodes.size();
    Primitives::PrimitivesFakeballs *dst = &primitives.fakeballs_p;
    int idx = 0;
    dst->id.clear();
    dst->index.clear();
    dst->centerPosition.clear();
    dst->imposterPosition.clear();
    dst->radius.clear();
    dst->color.clear();

    // Resize
    for(int i = 0; i < numNodes; i++){

        //id
        dst->id.push_back(scene.network.nodes[i].id);
        dst->id.push_back(scene.network.nodes[i].id);
        dst->id.push_back(scene.network.nodes[i].id);
        dst->id.push_back(scene.network.nodes[i].id);

        // index
        dst->index.push_back(idx);
        dst->index.push_back(idx + 1);
        dst->index.push_back(idx + 2);
        dst->index.push_back(idx + 2);
        dst->index.push_back(idx + 1);
        dst->index.push_back(idx + 3);
        idx += 4;

        // 4 center pos
        dst->centerPosition.push_back(scene.network.nodes[i].position);
        dst->centerPosition.push_back(scene.network.nodes[i].position);
        dst->centerPosition.push_back(scene.network.nodes[i].position);
        dst->centerPosition.push_back(scene.network.nodes[i].position);

        // 4 corner of imposter  coor
        dst->imposterPosition.push_back(glm::vec2(-1, -1));
        dst->imposterPosition.push_back(glm::vec2(1, -1));
        dst->imposterPosition.push_back(glm::vec2(-1, 1));
        dst->imposterPosition.push_back(glm::vec2(1, 1));

        // 4 radius
        dst->radius.push_back(scene.network.nodes[i].size);
        dst->radius.push_back(scene.network.nodes[i].size);
        dst->radius.push_back(scene.network.nodes[i].size);
        dst->radius.push_back(scene.network.nodes[i].size);

        // 4 color
        dst->color.push_back(scene.network.nodes[i].color);
        dst->color.push_back(scene.network.nodes[i].color);
        dst->color.push_back(scene.network.nodes[i].color);
        dst->color.push_back(scene.network.nodes[i].color);

    }

    // Edges
    int numEdges = scene.network.edges.size();
    Primitives::PrimitivesFakelines *dst2 = &primitives.fakelines_p;
    idx = 0;
    dst2->id.clear();
    dst2->index.clear();
    dst2->p1.clear();
    dst2->p2.clear();
    dst2->imposterPosition.clear();
    dst2->width.clear();
    dst2->color.clear();

    for(int i = 0; i < numEdges; i++){
        
        // index
        dst2->index.push_back(idx);
        dst2->index.push_back(idx + 1);
        dst2->index.push_back(idx + 2);
        dst2->index.push_back(idx + 2);
        dst2->index.push_back(idx + 1);
        dst2->index.push_back(idx + 3);
        idx += 4;

        auto rp1 = scene.network.nodes[scene.network.edges[i].nid1].position;
        auto rp2 = scene.network.nodes[scene.network.edges[i].nid2].position;

        // 4  id
        dst2->id.push_back(scene.network.edges[i].id);
        dst2->id.push_back(scene.network.edges[i].id);
        dst2->id.push_back(scene.network.edges[i].id);
        dst2->id.push_back(scene.network.edges[i].id);

        // 4  pos 1
        dst2->p1.push_back(rp1);
        dst2->p1.push_back(rp1);
        dst2->p1.push_back(rp1);
        dst2->p1.push_back(rp1);
        
        // 4  pos 2
        dst2->p2.push_back(rp2);
        dst2->p2.push_back(rp2);
        dst2->p2.push_back(rp2);
        dst2->p2.push_back(rp2);

        // 4 corner of imposter  coor
        dst2->imposterPosition.push_back(glm::vec2(-1, -1));
        dst2->imposterPosition.push_back(glm::vec2(1, -1));
        dst2->imposterPosition.push_back(glm::vec2(-1, 1));
        dst2->imposterPosition.push_back(glm::vec2(1, 1));

        // 4 width
        dst2->width.push_back(scene.network.edges[i].size);
        dst2->width.push_back(scene.network.edges[i].size);
        dst2->width.push_back(scene.network.edges[i].size);
        dst2->width.push_back(scene.network.edges[i].size);

        // 4 color
        dst2->color.push_back(scene.network.edges[i].color);
        dst2->color.push_back(scene.network.edges[i].color);
        dst2->color.push_back(scene.network.edges[i].color);
        dst2->color.push_back(scene.network.edges[i].color);


    }

    // tex2screen
    Primitives::PrimitivesTex2Screen *dst3 = &primitives.tex2screen_p;
    dst3->position.clear();
    dst3->texcoor.clear();
    //std::cout << "Aspect Ration = " << aspectRatio << std::endl;
    dst3->position.push_back(glm::vec3(-aspectRatio, -1.0, 0.0));
    dst3->position.push_back(glm::vec3(-aspectRatio, 1.0, 0.0));
    dst3->position.push_back(glm::vec3(aspectRatio, 1.0, 0.0));
    dst3->position.push_back(glm::vec3(-aspectRatio, -1.0, 0.0));
    dst3->position.push_back(glm::vec3(aspectRatio, 1.0, 0.0));
    dst3->position.push_back(glm::vec3(aspectRatio, -1.0, 0.0));

    dst3->texcoor.push_back(glm::vec2(0.0, 0.0));
    dst3->texcoor.push_back(glm::vec2(0.0, 1.0));
    dst3->texcoor.push_back(glm::vec2(1.0, 1.0));
    dst3->texcoor.push_back(glm::vec2(0.0, 0.0));
    dst3->texcoor.push_back(glm::vec2(1.0, 1.0));
    dst3->texcoor.push_back(glm::vec2(1.0, 0.0));

}

#include "atlas/moc_atlas.cpp"