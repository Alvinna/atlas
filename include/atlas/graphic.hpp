#ifndef GRAPHIC_H
#define GRAPHIC_H

namespace Graphic
{
    class ShaderManager
    {
        public:
            std::shared_ptr<QOpenGLShaderProgram> new_program(std::string);
            bool remove_program(std::string);
            bool add_shader_file(std::string, QOpenGLShader::ShaderType, std::string);
            bool link_shader(std::string);
            std::shared_ptr<QOpenGLShaderProgram> get_program(std::string);
        private:
            std::unordered_map<std::string, std::shared_ptr<QOpenGLShaderProgram>> programs;
    };

    class BufferManager
    {
        public:
            std::shared_ptr<QOpenGLBuffer> new_buffer(std::string, QOpenGLBuffer::Type);
            bool remove_buffer(std::string);
            std::shared_ptr<QOpenGLBuffer> get_buffer(std::string);
        private:
            std::unordered_map<std::string, std::shared_ptr<QOpenGLBuffer>> buffers;
    };

    class ArrayManager
    {
        public:
            std::shared_ptr<QOpenGLVertexArrayObject> new_array(std::string);
            bool remove_array(std::string);
            std::shared_ptr<QOpenGLVertexArrayObject> get_array(std::string);
        private:
            std::unordered_map<std::string, std::shared_ptr<QOpenGLVertexArrayObject>> arrays;
    };

    class TextureManager
    {
        public:
            std::shared_ptr<QOpenGLTexture> new_texture(std::string, QOpenGLTexture::Target target);
            bool remove_texture(std::string);
            std::shared_ptr<QOpenGLTexture> get_texture(std::string);
        private:
            std::unordered_map<std::string, std::shared_ptr<QOpenGLTexture>> textures;
    };
    
    class FrameBufferManager
    {
        public:
            std::shared_ptr<QOpenGLFramebufferObject> new_framebuffer(std::string, int width, int height);
            std::shared_ptr<QOpenGLFramebufferObject> new_framebuffer(std::string, int width, int height, QOpenGLFramebufferObjectFormat format);
            bool remove_framebuffer(std::string);
            std::shared_ptr<QOpenGLFramebufferObject> get_framebuffer(std::string);
        private:
            std::unordered_map<std::string, std::shared_ptr<QOpenGLFramebufferObject>> framebuffers;
    };
};

#endif