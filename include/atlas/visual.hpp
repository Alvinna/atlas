#ifndef VISUAL_H
#define VISUAL_H

namespace Visual
{
    enum EdgeType{
        EDGE_DIRECTED,
        EDGE_UNDIRECTED
    };

    class Node
    {
        public:
            Node();
            Node(unsigned int id, glm::vec3 position = glm::vec3(0.0, 0.0, 0.0), 
                    glm::vec3 color = glm::vec3(0.5, 0.5, 0.5));
            unsigned int id;
            std::string label;
            glm::vec3 position;
            glm::vec3 color;
            float size;
        
    };
    
    class Edge
    {
        public:
            Edge();
            Edge(int id, int nid1, int nid2, EdgeType type, 
                    glm::vec3 color = glm::vec3(0.5, 0.5, 0.5));
            int id;
            int nid1;
            int nid2;
            EdgeType type;
            std::string label;
            glm::vec3 color;
            float size;
    };

    class Network
    {
        public:
            std::vector<Node>nodes;
            std::vector<Edge>edges;
    };

    class Scene
    {
        public:
           Scene();

            Network network;
            int idCounter = 0;
    };

};

#endif