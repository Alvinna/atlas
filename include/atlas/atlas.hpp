#ifndef ATLAS_H
#define ATLAS_H
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <atlas/graphic.hpp>
#include <atlas/visual.hpp>

#define NUM_LIGHT 1

// Data structure to hold drawing primitives
class Primitives
{
    public:
        class PrimitivesFakeballs
        {
            public:
                std::vector<int> index;
                std::vector<glm::vec3> centerPosition;
                std::vector<glm::vec2> imposterPosition;
                std::vector<float> radius;
                std::vector<glm::vec3> color;
                std::vector<unsigned int> id;
        }fakeballs_p;
        class PrimitivesFakelines
        {
            public:
                std::vector<int> index;
                std::vector<glm::vec3> p1;
                std::vector<glm::vec3> p2;
                std::vector<glm::vec2> imposterPosition;
                std::vector<float> width;
                std::vector<glm::vec3> color;
                std::vector<unsigned int> id;
        }fakelines_p;
        class PrimitivesTex2Screen
        {
            public:
                std::vector<glm::vec3> position;
                std::vector<glm::vec2> texcoor;
        }tex2screen_p;


};

// Main render window
class RenderView : public QOpenGLWindow,  protected QOpenGLExtraFunctions
{
    Q_OBJECT
    
    Graphic::ArrayManager array_mgr;
    Graphic::BufferManager buffer_mgr;
    Graphic::ShaderManager shader_mgr;
    Graphic::TextureManager texture_mgr;
    Graphic::FrameBufferManager framebuffer_mgr;
    Visual::Scene scene;
    Primitives primitives;

    // view
    glm::vec3 cameraCenter;
    glm::mat4 cameraRotation;
    float cameraDistance;


;

    // perspective
    float FOV;
    float aspectRatio;
    float clippingPlaneNear;
    float clippingPlaneFar;

    // mouse
    enum {
        MOUSE_IDLE,
        MOUSE_MOVEXY,
        MOUSE_MOVEZ,
        MOUSE_ROTATE
    }mouseMode;
    glm::ivec2 mousePosition;

    // light
    struct Lighting{
        glm::vec3 Ia;
        struct LightSource{
            glm::vec3 position;
            glm::vec3 Is;
            glm::vec3 Id;
        } lights[NUM_LIGHT];        
    }lighting;
    // material (use global currently)
    struct Material{
        glm::vec3 Ka;
        glm::vec3 Ks;
        glm::vec3 Kd;
        float alpha;
    }material;

    // selection buffer
    unsigned int* selectionBuffer;
    unsigned int selection;
    GLuint screenFBO;
    GLuint screenTex;
    GLuint screenDepth;


    public slots:    
        void initializeGL() override;
        void resizeGL(int w, int h) override;
        void paintGL() override;

    public:
        virtual void mouseMoveEvent(QMouseEvent *event);
        virtual void mousePressEvent(QMouseEvent *event);
        virtual void mouseReleaseEvent(QMouseEvent *event);

    private:
        void generatePrimitives(); // This function should convert a scene to primitives 
};


#endif