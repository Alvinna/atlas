#include <QtCore>
#include <QtGui>
#include <QtOpenGL>
#include <atlas/atlas.hpp>

// Will be replace with widget app and embed the QWindow later
int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QSurfaceFormat format;
    format.setSamples(4);
    format.setSwapInterval(0);
    format.setDepthBufferSize(32);
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);

    RenderView window;
    window.setFormat(format);
    window.resize(640, 480);
    window.show();

    return app.exec();
}