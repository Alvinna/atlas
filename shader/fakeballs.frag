#version 330 core
#define NUM_LIGHT 1
// Input
in highp vec2 g_imposterPosition;              //Imposter space coordinate
in highp vec4 g_centerPosition;               
flat in float g_radius;
flat in vec3 g_color;
flat in uint g_id;

// Structure
struct LightSource{
        vec3 position;
        vec3 Is;
        vec3 Id;
};
struct Lighting{
    vec3 Ia;
    LightSource lights[NUM_LIGHT];        
};
uniform struct Material{
    vec3 Ka;
    vec3 Ks;
    vec3 Kd;
    float alpha;
};


// Global
uniform mat4 mv_matrix;
uniform mat4 p_matrix;
uniform mat4 o_matrix;
uniform Lighting lighting;
uniform Material material;

// Output
layout(location = 0) out highp uvec2 out_color_id;
//layout(location = 1) out highp uvec4 out_id;

void main(){
    highp float nd2 = g_imposterPosition.x * g_imposterPosition.x + 
        g_imposterPosition.y * g_imposterPosition.y;
    highp float ndz = sqrt(1.0f - nd2);
    highp float ndx = g_imposterPosition.x;
    highp float ndy = g_imposterPosition.y; 

    // Calculate depth
    vec4 pos = g_centerPosition;
    pos.z += ndz * g_radius;
    vec4 tpos = p_matrix * pos;
    highp float far=gl_DepthRange.far; float near=gl_DepthRange.near;
    highp float ndc_depth = (tpos.z) / tpos.w;
    highp float real_depth = (((far-near) * ndc_depth) + near + far) / 2.0;
    gl_FragDepth = real_depth;

    if(nd2 > 1.0)
        discard;

    // Calculate color
    vec3 normal = vec3(ndx, ndy, ndz);
    vec3 position = g_centerPosition.xyz + vec3(g_radius) * normal;
    vec3 V = normalize(-position);
    vec3 intensity = lighting.Ia * material.Ka;

    for(int i = 0; i < NUM_LIGHT; i++){
        //vec3 lightpos = (mv_matrix * vec4(lighting.lights[i].position, 1.0)).xyz; // This line determine whether the lights are fixed
        vec3 lightpos = lighting.lights[i].position;
        vec3 L = normalize(lightpos - position);
        float NL = clamp(dot(L, normal), 0.0, 1.0);
        vec3 R = 2 * NL * normal - L;
        intensity += material.Kd * NL * lighting.lights[i].Id;
        intensity += material.Ks * pow(clamp(dot(V, R), 0.0, 1.0), material.alpha) * lighting.lights[i].Is;

    }

   
    vec4 tf = 255 * vec4(g_color, 1.0) * clamp(vec4(intensity, 1.0), 0.0, 1.0);
    uvec4 tc = uvec4(tf);
    uint c = (tc.r) | (tc.g << 8) | (tc.b << 16) | (tc.a << 24);
    out_color_id = uvec2(c, g_id);
}