#version 330 core
#define NUM_LIGHT 1
// Input
in highp vec2 g_imposterPosition;              //Imposter space coordinate
in highp vec4 g_p1;               
in highp vec4 g_p2;               
flat in float g_width;
in vec3 g_color;
flat in float g_ratio;
in vec3 g_offset;
in vec3 g_cornerPosition;
flat in uint g_id;
in vec3 g_dir;

// Structure
struct LightSource{
        vec3 position;
        vec3 Is;
        vec3 Id;
};
struct Lighting{
    vec3 Ia;
    LightSource lights[NUM_LIGHT];        
};
uniform struct Material{
    vec3 Ka;
    vec3 Ks;
    vec3 Kd;
    float alpha;
};


// Global
uniform mat4 mv_matrix;
uniform mat4 p_matrix;
uniform mat4 o_matrix;
uniform Lighting lighting;
uniform Material material;

// Output
out uvec2 out_color_id;

void main(){
    float dx = sqrt(1.0f - g_imposterPosition.x * g_imposterPosition.x);
    vec4 pos = vec4(g_cornerPosition, 1);

    vec2 np = g_imposterPosition;
    np.y /= g_ratio;
    float cd;
    if( g_imposterPosition.y >= 0.0){
        cd = length(np - vec2(0.0, 1.0));
        if( g_imposterPosition.y > 1.0 && cd > 1.0){
                discard;
        }
        else{
            pos.z += (dx * g_width / g_ratio);
        }
    }
    else{
        cd = length(np - vec2(0.0, -1.0));
        if( g_imposterPosition.y < -1.0){
            if(cd > 1.0)
                discard;
            else{
           // disk
                pos.z += (np.y * g_width / g_ratio);
            }
        }
        else{
            if(cd > 1.0){
            // tube
                pos.z += (dx * g_width / g_ratio);
            }
            else{
            //disk
                pos.z += (np.y * g_width / g_ratio);
            }
        }
    }

    

    vec4 tpos = p_matrix * pos;
    highp float far=gl_DepthRange.far; float near=gl_DepthRange.near;
    highp float ndc_depth = (tpos.z) / tpos.w;
    highp float real_depth = (((far-near) * ndc_depth) + near + far) / 2.0;
    gl_FragDepth = real_depth;
    

    // Calculate color
    
    vec3 normal = g_offset;
    vec3 position = g_cornerPosition.xyz; //+ vec3(g_width) * normal;
    vec3 V = normalize(-position);
    vec3 intensity = lighting.Ia * material.Ka;

    for(int i = 0; i < NUM_LIGHT; i++){
        //vec3 lightpos = (mv_matrix * vec4(lighting.lights[i].position, 1.0)).xyz; // This line determine whether the lights are fixed
        vec3 lightpos = lighting.lights[i].position;
        vec3 L = normalize(lightpos - position);
        float NL = clamp(dot(L, normal), 0.0, 1.0);
        vec3 R = 2 * NL * normal - L;
        intensity += material.Kd * NL * lighting.lights[i].Id;
        intensity += material.Ks * pow(clamp(dot(V, R), 0.0, 1.0), material.alpha) * lighting.lights[i].Is;

    }


    vec4 t_color = vec4(g_color, 1.0) * vec4(intensity, 1.0);
    vec4 tf = 255 * clamp(t_color, 0.0, 1.0);
    uvec4 tc = uvec4(tf);
    uint c = (tc.r) | (tc.g << 8) | (tc.b << 16) | (tc.a << 24);
    out_color_id = uvec2(c, g_id);
    //out_color = vec4(g_color, 1.0);
}