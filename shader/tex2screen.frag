#version 330 core

// Input
in vec2 g_texcoor;

// Global
uniform usampler2D screenTexture;
uniform uint selection;

// Output
out vec4 out_color;

void main(){
    uint tc = texture(screenTexture, g_texcoor).r;
    uint ts = texture(screenTexture, g_texcoor).g;


    vec4 t_color = vec4(float((tc) & uint(0xff))/255.0, float((tc >> 8) & uint(0xff))/255.0, 
                        float((tc >> 16) & uint(0xff))/255.0, float((tc >> 24) & uint(0xff))/255.0);

    if(selection != uint(0) && selection == ts){
        out_color = t_color * 0.6 + 0.4 * vec4(0.0, 0.0, 1.0, 0.0);
    }
    else{
        out_color = t_color;
    }
    //out_color = vec4(0.5, 0.5, 0.5, 1.0);
}