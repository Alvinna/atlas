#version 330 core

// Input
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texcoor; 

// Global
uniform mat4 o_matrix;

// Output
out vec2 g_texcoor;


void main(){
    gl_Position = o_matrix * vec4(position, 1.0);
    g_texcoor = texcoor;
}