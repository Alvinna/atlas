#version 330 core
#define NUM_LIGHT 1
// Input
in highp vec2 g_imposterPosition;              //Imposter space coordinate
in highp vec4 g_transformedCenterPosition;   
in highp vec4 g_centerPosition;               
flat in float g_radius;

// Structure
struct LightSource{
        vec3 position;
        vec3 Is;
        vec3 Id;
};
struct Lighting{
    vec3 Ia;
    LightSource lights[NUM_LIGHT];        
};
uniform struct Material{
    vec3 Ka;
    vec3 Ks;
    vec3 Kd;
    float alpha;
};


// Global
uniform mat4 mv_matrix;
uniform mat4 p_matrix;
uniform mat4 o_matrix;
uniform Lighting lighting;
uniform Material material;
uniform vec3 cameraPosition;

// Output
out vec4 color;

void main(){
    highp float nd2 = g_imposterPosition.x * g_imposterPosition.x + 
        g_imposterPosition.y * g_imposterPosition.y;
    highp float ndz = sqrt(1.0f - nd2);
    highp float ndx = g_imposterPosition.x;
    highp float ndy = g_imposterPosition.y; 

    // Calculate depth
    highp float far=gl_DepthRange.far; float near=gl_DepthRange.near;
    highp float ndc_depth = (g_transformedCenterPosition.z) / g_transformedCenterPosition.w;
    highp float real_depth = (((far-near) * ndc_depth) + near + far) / 2.0;
    //highp float real_depth = ((g_centerPosition.z - g_radius*ndz) - near)/(far - near);
    gl_FragDepth = real_depth;

    if(nd2 > 1.0)
        discard;

    // Calculate color
    vec3 normal = vec3(ndx, ndy, ndz);
    vec3 position = g_centerPosition.xyz + vec3(g_radius) * normal;
    vec3 V = normalize(cameraPosition - position);
    vec3 intensity = lighting.Ia * material.Ka;
    for(int i = 0; i < NUM_LIGHT; i++){
        vec3 lightpos = (mv_matrix * vec4(lighting.lights[i].position, 1.0)).xyz; // This line determine whether the lights are fixed
        vec3 L = normalize(lightpos - position);
        vec3 R = 2 * dot(L, normal) * normal - L;
        intensity += material.Kd * dot(L, normal) * lighting.lights[i].Id;
        intensity += material.Ks * pow(dot(V, R), material.alpha) * lighting.lights[i].Is;

    }



    color = vec4(0.6039, 0.9412, 1.0, 1.0) * vec4(intensity, 1.0);
    //color = vec4(1.0, 1.0, 1.0, 1.0) * real_depth;
}