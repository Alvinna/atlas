#version 330 core

// Input
layout(location = 0) in vec3 centerPosition;
layout(location = 1) in vec2 imposterPosition; 
layout(location = 2) in float radius;
layout(location = 3) in vec3 color;
layout(location = 4) in uint id;

// Global
uniform mat4 mv_matrix;
uniform mat4 p_matrix;
uniform mat4 o_matrix;

// Output
out highp vec2 g_imposterPosition;              //Imposter space coordinate
out highp vec4 g_centerPosition;             
flat out float g_radius;
flat out vec3 g_color;
flat out uint g_id;

void main(){

    highp vec4 cornerPosition;
    g_centerPosition = mv_matrix * vec4(centerPosition, 1);
    cornerPosition = g_centerPosition + vec4(imposterPosition * vec2(radius),0, 1);
    g_imposterPosition = imposterPosition;
    gl_Position = p_matrix * cornerPosition;
    g_radius = radius;
    g_color = color;
    g_id = id;
  
}