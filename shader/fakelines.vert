#version 330 core

// Input
layout(location = 0) in vec3 p1;
layout(location = 1) in vec3 p2;
layout(location = 2) in vec2 imposterPosition; 
layout(location = 3) in float width;
layout(location = 4) in vec3 color;
layout(location = 5) in uint id;

// Global
uniform mat4 mv_matrix;
uniform mat4 p_matrix;
uniform mat4 o_matrix;

// Output
out highp vec2 g_imposterPosition;              //Imposter space coordinate
out highp vec4 g_p1;             
out highp vec4 g_p2;             
flat out float g_width;
out vec3 g_color;
flat out float g_ratio;
out vec3 g_offset;
out vec3 g_cornerPosition;
flat out uint g_id;
out vec3 g_dir;

void main(){

    highp vec4 cornerPosition;
    vec4 t_p1 = mv_matrix * vec4(p1, 1);
    vec4 t_p2 = mv_matrix * vec4(p2, 1);
    if( t_p1.z >= t_p2.z ){ // p1 is nearer
        g_p1 = t_p1;
        g_p2 = t_p2;
    }
    else{
        g_p1 = t_p2;
        g_p2 = t_p1;
    }

    vec3 dir = g_p2.xyz - g_p1.xyz;
    vec3 ndir = normalize(dir);
    g_dir = dir;
    vec3 noffset = imposterPosition.x * cross(g_p1.xyz + g_p2.xyz, ndir); // determine offset direction
    if (length(noffset) != 0.0)
        noffset = normalize(noffset);
    else
        noffset = vec3(0.0, 0.0, 0.0);

    g_ratio = sqrt(1 - ndir.z * ndir.z);
    float r = 2 * width * g_ratio / length(dir);
    if (imposterPosition.y > 0){
        g_imposterPosition = imposterPosition;
        g_imposterPosition.y += r;
        cornerPosition = p_matrix * g_p2;
        cornerPosition.xyz += noffset * width;
        cornerPosition.xyz += ndir * g_ratio * width;
    }
    else{
        g_imposterPosition = imposterPosition;
        g_imposterPosition.y -= r;
        cornerPosition = p_matrix * g_p1;
        cornerPosition.xyz += noffset * width;
        cornerPosition.xyz -= ndir * g_ratio * width;
    }
    
    gl_Position = p_matrix * cornerPosition;
    g_cornerPosition = cornerPosition.xyz;
    g_width = width;
    g_color = color;
    g_offset = noffset;
    g_id = id;
}