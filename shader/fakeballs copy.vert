#version 330 core

// Input
layout(location = 0) in vec3 centerPosition;
layout(location = 1) in vec2 imposterPosition; 
layout(location = 2) in float radius;

// Global
uniform mat4 mv_matrix;
uniform mat4 p_matrix;
uniform mat4 o_matrix;

// Output
out highp vec2 g_imposterPosition;              //Imposter space coordinate
out highp vec4 g_transformedCenterPosition;
out highp vec4 g_centerPosition;             
flat out float g_radius;

void main(){

    highp vec4 transformedCenterPosition;
    highp vec4 cornerPosition;
    highp vec4 orthoCorner;
    transformedCenterPosition = p_matrix * mv_matrix * vec4(centerPosition, 1);
    orthoCorner = o_matrix * vec4(imposterPosition * vec2(radius),0, 1);
    cornerPosition = transformedCenterPosition;
    cornerPosition.xy = cornerPosition.xy + orthoCorner.xy;
    g_imposterPosition = imposterPosition;
    g_transformedCenterPosition = transformedCenterPosition;
    g_centerPosition = mv_matrix * vec4(centerPosition, 1);
    gl_Position = cornerPosition;
    g_radius = radius;
  
}